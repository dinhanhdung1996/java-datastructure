package Tree;

import java.util.HashMap;
import java.util.Map;
import SingleList.*;
/**
 * Created by Dung Dinh on 7/10/2017.
 */
public class TrieNode extends Node {
    private boolean isLeaf = false;
    private char c;
    HashMap<Character, TrieNode> children;

    public TrieNode(){
        children = new HashMap<>();

    }

    public void setChildren(HashMap<Character, TrieNode> children){
        this.children = children;
    }

    public HashMap<Character, TrieNode> getChildren(){
        return children;
    }


    public int numberOfChildren(){
        int count = 0;
        for(Map.Entry<Character, TrieNode> entry: children.entrySet()){
            count++;
        }
        return count;
    }

    public void deleteNode(char c){
        children.remove(c);
    }

    public TrieNode(char c){
        this.c = c;
        children = new HashMap<>();
    }

    public void setC(char c){
        this.c = c;
    }

    public char getC(){
        return c;
    }

    public void setLeaf(boolean isLeaf){
        this.isLeaf = isLeaf;
    }

    public boolean isLeaf(){
        return isLeaf;
    }

    public String toString(){
        return ""+c + "--" + children;
    }
}
