package Tree;

/**
 * Created by Dung Dinh on 7/7/2017.
 */
public class BinarySearchTree<T extends Comparable<T>> {

    private TreeNode<T> root; //root node

    public BinarySearchTree(){
        root = null;
    }

    public BinarySearchTree(T data){
        root = new TreeNode<>(data);
    }

    public TreeNode<T> getRoot(){
        return root;
    }

    public void setRoot(TreeNode<T> root){
        this.root = root;
    }

    private TreeNode<T> insertNode(TreeNode<T> root, T data){ //insert a node with a given data
        if(root == null){
            root = new TreeNode<>(data);
        }
        if(data.compareTo((T)root.getData()) == 0)return root;
        else if(data.compareTo((T)root.getData()) < 0){
            root.setLeft(insertNode(root.getLeft(), data));
        }
        else if(data.compareTo((T)root.getData()) > 0){
            root.setRight(insertNode(root.getRight(), data));
        }
        return root;

    }

    public void insertNode(T data){
        root = insertNode(root, data);
    }

    private TreeNode<T> searchNode(TreeNode<T> node, T data){ //search a node with a given data
        if(root == null)return null;
        if(node == null)return null;
        if(data.compareTo((T)node.getData())  == 0)return node;
        else if(data.compareTo((T)node.getData()) > 0)return searchNode(node.getRight(), data);
        else if(data.compareTo((T)node.getData()) < 0 )return searchNode(node.getLeft(), data);
        return node;
    }

    public TreeNode<T> searchNode(T data){
        return searchNode(this.root, data);
    } // search a node with given data

    private TreeNode<T> deleteNode(TreeNode<T> node, T data){ // delete node with given data
        if(node == null)return node;
        if(data.compareTo((T)node.getData()) > 0) node.setRight(deleteNode(node.getRight(), data));
        else if(data.compareTo((T)node.getData()) < 0) node.setLeft(deleteNode(node.getLeft(), data));
        else {

            if(node.getLeft() == null){
                return node.getRight();
            } else if(node.getRight() == null){
                return node.getLeft();
            }

            node.setData(findMin(node.getRight()).getData());
            node.setRight(deleteNode(node.getRight(), (T)node.getData()));
        }

        return node;
    }

    public void deleteNode(T data){
        root = deleteNode(root, data);
    } // delete node with given data

    public TreeNode<T> findMin(TreeNode<T> node){ // find node with smallest data
        TreeNode<T> temp = node;
        while(temp.getLeft() != null){
            temp = temp.getLeft();
        }
        return temp;
    }

    public void inorderTraversal(TreeNode<T> node){
        if(node == null)return;
        if(node.getLeft() != null)inorderTraversal(node.getLeft());
        System.out.println(node.getData());
        if(node.getRight() != null)inorderTraversal(node.getRight());
    }
    public void inorderTraversal(){
        inorderTraversal(root);
    }


    public void preorderTraversal(TreeNode<T> node){
        if(node == null)return;
        System.out.println(node.getData());
        if(node.getLeft() != null)preorderTraversal(node.getLeft());
        if(node.getRight() != null)preorderTraversal(node.getRight());
    }


    public void preorderTraversal(){
        preorderTraversal(root);
    }

    public void postorderTraversal(TreeNode<T> node){
        if(node == null)return;
        if(node.getLeft() != null)postorderTraversal(node.getLeft());
        if(node.getRight() != null)postorderTraversal(node.getRight());
        System.out.println(node.getData());
    }

    public void postorderTraversal(){
        postorderTraversal(root);
    }

    public int heightTree(TreeNode<T> node){
        if(node == null)return 0;
        int leftHeight = 0;
        int rightHeight = 0;
        leftHeight = heightTree(node.getLeft());
        rightHeight = heightTree(node.getRight());

        if(leftHeight >= rightHeight)return  leftHeight+1;
        else return rightHeight+1;
    }

    public void BFSTraversal(){
        int height= heightTree(root);
        if(height == 0)return;

        for(int i = 1; i<= height; i++){
            printLevel(root, i);
        }
    }

    public  void printLevel(TreeNode<T> node, int i){
        if(node == null)return;
        if(i == 1){
            System.out.println(node.getData());
        } else{
            printLevel(node.getLeft(), i-1);
            printLevel(node.getRight(), i -1);
        }
    }
}
