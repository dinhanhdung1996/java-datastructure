package Tree;

import java.util.HashMap;

/**
 * Created by Dung Dinh on 7/10/2017.
 */
public class Trie<T extends Comparable<T>> {
    private TrieNode root;

    public Trie(){
        root = new TrieNode((char) 0);
    }

    public void insertNode( String string){
        HashMap<Character, TrieNode> children = root.getChildren();
        TrieNode node = root;
        for(int i = 0; i< string.length(); i++){
            char c = string.charAt(i);
            if(children.containsKey(c)){
                node = children.get(c);
            }
            else
            {
                node = new TrieNode(c);
                children.put(c, node);
            }
            children = node.getChildren();

            if( i == string.length() - 1){
                node.setLeaf(true);
            }
        }
    }

    public TrieNode searchNode(String string){
        HashMap<Character, TrieNode> children = root.getChildren();
        TrieNode node = root;
        for(int i = 0; i< string.length(); i++){
            char c = string.charAt(i);
            if(!children.containsKey(c))return null;
            node = children.get(c);
            children = node.getChildren();
        }
        return node;
    }

    public boolean search(String string){
        TrieNode temp = searchNode(string);
        if(temp != null && temp.isLeaf())return true;
        else return false;
    }

    public TrieNode searchPrev(String prefix){
        TrieNode temp = searchNode(prefix);
        if(temp != null)return temp;
        else return null;
    }


    public boolean inDependent(String string){
        TrieNode temp = root;
        HashMap<Character, TrieNode> children = temp.getChildren();
        for(int i = 0; i < string.length(); i++)
        {
            char c = string.charAt(i);
            if(!children.containsKey(c))return false;
            temp = children.get(c);
            if(i != string.length()-1 && temp.isLeaf())return false;
            children = temp.getChildren();
            if(temp.numberOfChildren() >= 2)return false;
        }
        return true;
    }

    public boolean totallyDependent(String string){
        TrieNode trieNode = searchNode(string);
        if(trieNode== null)return false;
        if(trieNode.numberOfChildren() != 0 && trieNode.isLeaf())return true;
        else return false;
    }


    public void deleteString(String string){
        HashMap<Character, TrieNode> children = root.getChildren();
        if(!search(string))return;
        if(inDependent(string)){
            //System.out.println(inDependent(string));
            children.remove(string.charAt(0));
            return;
        }
        if(totallyDependent(string)){
            //System.out.println(totallyDependent(string));
            TrieNode trieNode = searchNode(string);
            trieNode.setLeaf(false);
            return;
        }
        TrieNode temp = root;
        TrieNode temp2 = null;
        TrieNode temp3 = null;
        boolean next = false;
        for(int i = 0; i < string.length(); i++){
            char c = string.charAt(i);
            if(!children.containsKey(c))return;
            temp = children.get(c);
            if(temp.numberOfChildren() > 1 || (temp.isLeaf() && i!= string.length()-1)){
                next = true;
                temp3 = temp;
                System.out.println("temp3: " + temp3);
                children = temp.getChildren();
                continue;
            }
            if(next == true){
                temp2 = temp;
                next = false;
            }
            children = temp.getChildren();
        }

        temp2.setLeaf(false);
        temp2.getChildren().clear();
        temp3.getChildren().remove(temp2.getC());
    }
}
