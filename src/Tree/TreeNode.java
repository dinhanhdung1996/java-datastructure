package Tree;
import SingleList.*;
/**
 * Created by Dung Dinh on 7/7/2017.
 */
public class TreeNode <N extends Comparable<N>> extends Node {
    TreeNode left;
    TreeNode right;
    public TreeNode(N data){
        super(data);
        left = null;
        right = null;

    }

    public TreeNode(){
        left = null;
        right = null;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public void setLeft(N data){
        TreeNode<N> a = new TreeNode<>(data);
        setLeft(a);
    }

    public void setRight(N data){
        TreeNode<N> a = new TreeNode<>(data);
        setRight(a);
    }
    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }

    public int compareTo(TreeNode<N> data){
        return this.getData().compareTo(data);
    }
}
