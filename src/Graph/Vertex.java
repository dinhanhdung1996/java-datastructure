package Graph;

import java.util.ArrayList;

/**
 * Created by Dung Dinh on 6/30/2017.
 */
public class Vertex implements Comparable<Vertex>{
    private static final double HIGH = 999999999999999999999999.0;
    private static final double LOW = -999999999999999999999999.0;
    private String vertexName;
    private int vertexNum = -1 ;
    private ArrayList<Edge> adjacency = new ArrayList<>();

    public void setVertexNum(int vertexNum){
        vertexNum = this.vertexNum;
    }

    public int getVertexNum(){
        return vertexNum;
    }

    public String getVertexName() {
        return vertexName;
    }

    public void setVertexName(String vertexName) {
        this.vertexName = vertexName;
    }

    public void addEdge(Vertex a, boolean dir){
        for(Edge b: adjacency){
            if(b.getEndVertex().equals(a))return;
        }
        Edge edge = new Edge();
        edge.connect(this, a);
        adjacency.add(edge);
        if(dir == true)
        {
            a.addEdge(this, false);
        }
    }


    public void addEdge(Vertex a, double weight, boolean dir){
        for(Edge b: adjacency){
            if(b.getEndVertex().equals(a))return;
        }
        Edge edge = new Edge();
        edge.connect(this, a, weight);
        adjacency.add(edge);
        if(dir == true){
            a.addEdge(this, weight, false);
        }
    }

    public boolean checkAdjacency(Vertex a){
        for(Edge b: adjacency){
            if(b.getEndVertex().equals(a)){
                return true;
            }
        }
        return  false;
    }

    public int compareTo(Vertex vertex){
        return getVertexName().compareTo(vertex.getVertexName());
    }

    public boolean equals(Vertex vertex){
        if(this.getVertexName().equals(vertex.getVertexName())){
            return true;
        }
        else if(this.getVertexNum() == vertex.getVertexNum() && this.getVertexNum() != -1)return true;
        else return false;
    }


    public ArrayList<Vertex> getAdjacency(){
        ArrayList<Vertex> vertices= new ArrayList<>();
        for(Edge edge: adjacency){
            vertices.add(edge.getEndVertex());
        }
        return vertices;
    }

    public double getWeight(Vertex adjacent){
        if(!checkAdjacency(adjacent))return HIGH;
        for(Edge edge : adjacency){
            if(edge.getEndVertex().equals(adjacent)){
                return edge.getWeight();
            }
        }
        return HIGH;
    }

    public String toString(){
        return getVertexName();
    }

}
