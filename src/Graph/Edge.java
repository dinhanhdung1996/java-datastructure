package Graph;

/**
 * Created by Dung Dinh on 7/1/2017.
 */
public class Edge {
    private Vertex vertex1;
    private Vertex vertex2;
    private double weight;


    public void connect(Vertex vertex1, Vertex vertex2){
       connect(vertex1, vertex2, 1);

    }

    public void connect(Vertex vertex1, Vertex vertex2, double weight){
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.weight = weight;
    }


    public Vertex getEndVertex(){
        return vertex2;
    }

    public Vertex getStartVertex(){
        return vertex1;
    }

    public double getWeight(){
        return  weight;
    }



}
