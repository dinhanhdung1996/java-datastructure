package Graph;

import java.util.ArrayList;
import java.util.HashMap;
import SingleList.*;

public class Dijkstra <V extends Vertex> {
    private static final double HIGH = 9999999999.0;
    private static final double LOW = -9999999999.0;

    private Graph<V> graph;
    private HashMap<Integer, Integer> predecessors;
    private HashMap<V , Double> distance;


    public Graph<V> getGraph() {
        return graph;
    }

    public void setGraph(Graph<V> graph) {
        this.graph = graph;
    }

    public HashMap<Integer, Integer> getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(HashMap<Integer, Integer> predecessors) {
        this.predecessors = predecessors;
    }

    public HashMap<V, Double> getDistance() {
        return distance;
    }

    public void setDistance(HashMap<V, Double> distance) {
        this.distance = distance;
    }


    public Dijkstra(){
        predecessors = new HashMap<>();
        distance = new HashMap<>();
    }

    private class VertexWeight implements Comparable<VertexWeight>{
        public V vertex;
        public double weight = HIGH;
        public int compareTo(VertexWeight vertexWeight){
            return (int)(weight - vertexWeight.weight);
        }
    }

    public VertexWeight getMinimum(SingleLinkList<VertexWeight> queue){
        VertexWeight min = (VertexWeight)queue.get(0).getData();
        ListNode<VertexWeight>temp = queue.get(0);
        while(temp!= null){
            if(min.compareTo((VertexWeight)temp.getData()) > 0){
                min = (VertexWeight)temp.getData();
            }
            temp = temp.getNext();
        }
        return min;
    }

    public void dijkstra(V start, V end){
        VertexWeight startWeight = new VertexWeight();
        startWeight.vertex = start;
        startWeight.weight = 0;
        SingleLinkList<VertexWeight> queue = new SingleLinkList<VertexWeight>();
        queue.insertAtCurrent(startWeight);
        ArrayList<Boolean> visit = new ArrayList<>();
        for(Vertex vertex: graph.getVertices()){
            visit.add(new Boolean(false));
        }
        while (!queue.isEmpty()){
            VertexWeight vertexWeight = getMinimum(queue);
            queue.deleteNode(vertexWeight);
            int index1 = graph.getVertices().indexOf(vertexWeight.vertex);
            visit.set(index1, new Boolean(true));
            for(Vertex vertex : graph.getAdjacentVertices(vertexWeight.vertex)){
                int index2 = graph.getAdjacentVertices(vertexWeight.vertex).indexOf(vertex);
                VertexWeight vertexWeight1 = new VertexWeight();
                vertexWeight1.vertex = (V)vertex;
                double d  = vertexWeight.weight + vertexWeight.vertex.getWeight(vertex);
                if (d < vertexWeight.weight){
                    vertexWeight1.weight = d;

                }
            }
        }
    }
}
