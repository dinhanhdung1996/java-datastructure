package Graph;
import java.util.ArrayList;
import java.util.HashMap;
import SingleList.*;
/**
 * Created by Dung Dinh on 7/1/2017.
 */
public class Graph <V extends Vertex>{

    private ArrayList<V> vertices = new ArrayList<>();

    public V getVertex(String name){
        for(V v: vertices){
            if(v.getVertexName().equals(name))return v;
        }
        return null;
    }

    public V getVertex(int num){
        for(V v :vertices){
            if(v.getVertexNum() == num)return v;
        }
        return null;
    }
    public ArrayList<V> getVertices(){
        return vertices;
    }

    public void setVertices(ArrayList<V> vertices){
        this.vertices = new ArrayList<>(vertices);
    }

    public ArrayList<V>getAdjacentVertices(V vertex){
        return (ArrayList<V>) vertex.getAdjacency();
    }

    public boolean addEdge(V vertex1, V vertex2, boolean dir){
        if(addEdge(vertex1, vertex2, 1, dir))return true;
        return false;
    }

    public boolean addEdge(V vertex1, V vertex2, double weight, boolean dir){
        if(!vertices.contains(vertex1))return false;
        if(!vertices.contains(vertex2))return false;
        vertex1.addEdge(vertex2, weight, dir);
        return true;
    }

    public void addVertex(V vertex){
        if(vertex == null)return;
        if(getVertex(vertex.getVertexName()) != null)return;
        if(getVertex(vertex.getVertexNum()) != null && vertex.getVertexNum() != -1 )return;
        vertices.add(vertex);
    }

    public void DFS(int v){
        System.out.println("DFS");
        if( v > vertices.size())return ;
        Stack<Vertex> stack = new Stack<>();

        ArrayList<Boolean> visit = new ArrayList<>();
        for(V a : vertices){
            visit.add(new Boolean(false));
        }
       // visit.set(0, new Boolean(true));
        stack.push(vertices.get(v));
        while(!stack.isEmpty()){
            Vertex vertex =stack.pop();
            System.out.println(vertex);
            int indexOfVertex = vertices.indexOf(vertex);
            if(indexOfVertex > visit.size()){
                System.out.println("out of size visit");
                return;
            }
            if(visit.get(indexOfVertex) == false) {
                //Graph.Vertex vertex = (Graph.Vertex)vertexListNode.getData();
                //System.out.println(vertices.get(indexOfVertex));
                visit.set(indexOfVertex, new Boolean(true));
                for (Vertex temp : vertex.getAdjacency()) {
                    int indexOfTemp = vertices.indexOf(temp);
                    if (indexOfTemp >= visit.size()) break;
                    if (visit.get(indexOfTemp)) continue;
                //    visit.set(indexOfTemp, true);
                    stack.push(temp);
                }
            }
        }
    }

    public void DFS(){
        DFS(0);
    }


    public void BFS(int v){
        System.out.println("BFS");
        Queue<Vertex> queue = new Queue<>();
        ArrayList<Boolean> visit = new ArrayList<>();
        if(v >= vertices.size())return;
        for(Vertex a: vertices){
            visit.add(new Boolean(false));
        }

        queue.enQueue(vertices.get(v));

        while(!queue.isEmpty()){
            Vertex vertex = queue.deQueue();
            int indexOfVertex = vertices.indexOf(vertex);
            if(indexOfVertex >= visit.size()){
                System.out.println("Out of range");
                return;
            }
            if(!visit.get(indexOfVertex)){
                System.out.println(vertex);
                visit.set(indexOfVertex, new Boolean(true));
                for(Vertex temp : vertex.getAdjacency()){
                    int indexOfTemp = vertices.indexOf(temp);
                    if(indexOfTemp >= visit.size())break;
                    if(visit.get(indexOfTemp) == false){
                        queue.enQueue(temp);
                    }
                }
            }
        }
    }

    public void BFS(){
        BFS(0);
    }

}
