package SingleList;

/**
 * Created by Dung Dinh on 7/6/2017.
 */
public class Node <T extends Comparable<T>>{
    private String stringData;
    private int intData;
    private double doubleData;
    private T data;
    public Node(){
    }

    public void setData(T data){
        this.data = data;
    }

    public T getData(){
        return data;
    }

    public String getStringData() {
        return stringData;
    }

    public void setStringData(String stringData) {
        this.stringData = stringData;
    }

    public Node(int intData){
        setIntData(intData);

    }

    public Node(String name){
        setStringData(name);
    }

    public Node(double doubleData){
        setDoubleData(doubleData);

    }

    public Node(String name, int intData){
        this.setStringData(name);
        this.setIntData(intData);

    }

    public Node(T data){
        setData(data);
    }

    public Node(String name, double doubleData){
        this.setStringData(name);
        this.setDoubleData(doubleData);

    }


    public int getIntData() {
        return intData;
    }

    public void setIntData(int intData) {
        this.intData = intData;
    }

    public double getDoubleData() {
        return doubleData;
    }

    public void setDoubleData(double doubleData) {
        this.doubleData = doubleData;
    }

    //public int compareKey(Node a){
//        return 0;
 //   }

    public String toString(){
        return ""+ data;
    }
}
