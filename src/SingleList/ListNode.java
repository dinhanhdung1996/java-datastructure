package SingleList;

/**
 * Created by Dung Dinh on 7/6/2017.
 */
public class ListNode <T extends Comparable<T>> extends Node {

    private ListNode next;
    public ListNode(T data){
        super(data);
        next = null;
    }
    public ListNode(){
        super();
        next = null;
    }


   public void setNext(ListNode listNode){
        next = listNode;
   }

   public ListNode getNext(){
       return next;
   }

   public String toString(){
       return "" + getData();
   }
}
