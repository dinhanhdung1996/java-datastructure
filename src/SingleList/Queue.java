package SingleList;

/**
 * Created by Dung Dinh on 7/9/2017.
 */
public class Queue <T extends Comparable<T>> extends SingleLinkList<T> {

    public void enQueue(T data){
        insertAtCurrent(data);
    }

    public T deQueue(){
        return (T)deleteRoot();
    }

}
