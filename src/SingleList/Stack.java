package SingleList;

/**
 * Created by Dung Dinh on 7/10/2017.
 */
public class Stack<T extends Comparable<T>> extends SingleLinkList<T> {

    public void push(T node){
        insertAtHead(node);
    }

    public T pop(){
        return deleteRoot();
    }
}
