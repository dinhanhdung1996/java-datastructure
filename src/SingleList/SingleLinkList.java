package SingleList;

/**
 * Created by Dung Dinh on 7/6/2017.
 */
public class SingleLinkList <N extends Comparable<N>> {
    private ListNode<N> root;
    private ListNode<N> end;
    //public N []list ;
    public SingleLinkList(){
        root = null;
        end = null;
    }

    public SingleLinkList(ListNode<N> firstNode){
        root = firstNode;
        end = firstNode;
    }


    public void insertAtHead(ListNode<N> node){
        if(root == null) {
            root = node;
            end = root;
            return;
        }

        node.setNext(root);
        root = node;

    }


    public void insertAtCurrent(ListNode<N> node){
        if(root == null){
            insertAtHead(node);
            end = root;
        }
        end.setNext(node);
        end = end.getNext();
    }

    public void insertAtCurrent(N data){
        ListNode<N> node = new ListNode<>(data);
        insertAtCurrent(node);

    }

    public void insertAtHead(N data){
        ListNode<N> node = new ListNode<>(data);
        insertAtHead(node);
    }

    public ListNode<N> get(int index){
        ListNode<N> temp = root;
        int count = 0;
        while(temp != null){
            temp = temp.getNext();
            count++;
            if(count == index)return temp;
        }
        return null;
    }


    public boolean insertAtIndex(ListNode<N> node, int index){
        ListNode<N> position = get(index);
        if(position == null)return false;
        node.setNext(position.getNext());
        position.setNext(node);
        return  true;
    }

    public int countNodes(){
        ListNode<N> temp = root;
        int count = 0;
        while(temp != null){
            count++;
            temp = temp.getNext();
        }
        return count;
    }

    public boolean contains(ListNode<N> node){
        ListNode<N> temp = root;
        while(temp != null){
            if(temp == node)return  true;
            temp = temp.getNext();
        }
        return false;
    }

    public ListNode<N> getPrevNode(ListNode<N> node){
        if(this.contains(node)){
            ListNode<N> temp = root;
            while(temp != null){
                if(temp.getNext() == node)return temp;
                temp = temp.getNext();
            }
        }
        return null;
    }

    public ListNode<N> getNextNode(ListNode<N> node){
        if(this.contains(node)){
            ListNode<N> temp = root;
            while(temp != null){
                if(temp == node)return temp.getNext();
                temp = temp.getNext();
            }
        }
        return null;
    }

    public N deleteRoot(){
        if(root == null)return null;
        N data= (N)root.getData();
        root = root.getNext();
        return data;
    }

    public N deleteCurrent(){
        if(end == null)return null;
        ListNode<N> prevCurrent = getPrevNode(end);
        if(prevCurrent == null){
            return deleteRoot();
        }
        N data = (N)prevCurrent.getData();
        end = prevCurrent;
        return data;
    }


    public N deleteNode(ListNode<N> node){
        if(this.contains(node)){
            if(node == end){
               return deleteCurrent();
            }
            ListNode<N> temp = getPrevNode(node);
            if(temp == null){
                return deleteRoot();
            }
            temp.setNext(node.getNext());
            N data = (N)node.getData();
            node = null;
            return data;
        }
        return null;
    }

    public N deleteNode(N data){
        ListNode<N>node = searchNode(data);
        return deleteNode(node);
    }

    public void deleteList(){
        root = null;
    }

    public ListNode<N> searchNode(N data){
        ListNode<N> temp = root;
        while(temp != null){
            if(temp.getData().equals(data)){
                return temp;
            }
        }
        return null;
    }

    public void setRoot(ListNode<N> node){
        root = node;
    }

    public ListNode<N> getRoot(){
        return root;
    }

    public String toString(){
        String a = "[";
        ListNode<N> temp = root;
        while(temp != null){
            a = a + temp.getData();
            temp = temp.getNext();
            if(temp != null) a = a + " , ";
        }

        a  = a + "]";
        return a;
    }

    public boolean isEmpty(){
        if(root == null) return true;
        else return false;
    }

}
